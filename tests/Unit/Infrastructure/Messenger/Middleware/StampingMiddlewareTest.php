<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Tests\Unit\Infrastructure\Messenger\Middleware\StampingMiddleware;

use Codeception\Test\Unit;
use Dvlpm\CommandBus\Infrastructure\Messenger\Middleware\StampingMiddleware;
use Dvlpm\CommandBus\Infrastructure\Messenger\Middleware\StampingMiddleware\StampProducerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\StampInterface;

/**
 * @group unit
 * @covers \Dvlpm\CommandBus\Infrastructure\Messenger\Middleware\StampingMiddleware
 */
class StampingMiddlewareTest extends Unit
{
    private StampingMiddleware $stampingMiddleware;
    private StampingMiddleware | MockObject $stampProducer;

    public function setUp(): void
    {
        $this->stampProducer = $this->createMock(StampProducerInterface::class);
        $this->stampingMiddleware = new StampingMiddleware($this->stampProducer);
    }

    public function testProducingStamps(): void
    {
        $command = new class() {
        };
        $envelope = Envelope::wrap($command);
        $stack = $this->createStack();

        $stamp1 = $this->createMock(StampInterface::class);
        $stamp2 = $this->createMock(StampInterface::class);

        $this->stampProducer
            ->method('produceStamps')
            ->willReturn([
                $stamp1,
                $stamp2,
            ]);

        $result = $this->stampingMiddleware->handle($envelope, $stack);

        $this->assertNotNull($result->last(get_class($stamp1)));
        $this->assertNotNull($result->last(get_class($stamp2)));
    }

    private function createStack(): StackInterface
    {
        $stack = $this->createMock(StackInterface::class);
        $stack->expects($this->once())
            ->method('next')
            ->willReturn(
                new class() implements MiddlewareInterface {
                    public function handle(
                        Envelope $envelope,
                        StackInterface $stack
                    ): Envelope {
                        return $envelope;
                    }
                }
            );

        return $stack;
    }
}
