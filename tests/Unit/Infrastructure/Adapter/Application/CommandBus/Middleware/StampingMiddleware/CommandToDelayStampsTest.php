<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Tests\Unit\Infrastructure\Adapter\Application\CommandBus\Middleware\StampingMiddleware;

use Codeception\Test\Unit;
use Dvlpm\CommandBus\Infrastructure\Adapter\Application\CommandBus\Middleware\StampingMiddleware\CommandToDelayStamps;
use Dvlpm\CommandBus\Tests\Support\Builder\ScopeBuilder;
use Dvlpm\CommandBus\Tests\Support\Command\TestHasScopeCommand;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;

/**
 * @group unit
 * @covers \Dvlpm\CommandBus\Infrastructure\Adapter\Application\CommandBus\Middleware\StampingMiddleware\CommandToDelayStamps
 */
class CommandToDelayStampsTest extends Unit
{
    private const MAX_DELAY = 2000;

    private CommandToDelayStamps $delayStamps;

    public function setUp(): void
    {
        $this->delayStamps = new CommandToDelayStamps([
            TestHasScopeCommand::class => [
                'delay' => 1,
                'max_delay' => 100,
                'multiplier' => 5,
            ],
        ]);
    }

    public function testMaxDelayCalculation(): void
    {
        $envelope = Envelope::wrap(
            new TestHasScopeCommand(ScopeBuilder::create()->build()),
            [new DelayStamp(self::MAX_DELAY), new DelayStamp(5)]
        );

        $stamps = $this->delayStamps->produceStamps($envelope);

        assert(count($stamps) === 1);
        $this->assertEquals(self::MAX_DELAY, current($stamps)->getDelay());
    }

    public function testScopeDelayCalculation(): void
    {
        $command = new TestHasScopeCommand(ScopeBuilder::create()->build());
        $expectedDelays = [1, 5, 25, 100,];

        foreach ($expectedDelays as $expectedDelay) {
            $stamps = $this->delayStamps->produceStamps(Envelope::wrap($command));
            assert(count($stamps) === 1);
            $this->assertEquals($expectedDelay, current($stamps)->getDelay());
            $command->getScope()->incrementRetryCount();
        }

        for ($i = 0; $i < 10; ++$i) {
            $command->getScope()->incrementRetryCount();
        }
        $stamps = $this->delayStamps->produceStamps(Envelope::wrap($command));

        assert(count($stamps) === 1);
        $this->assertEquals(100, current($stamps)->getDelay());
    }
}
