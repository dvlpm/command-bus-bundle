<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Tests\Support\Command;

use Dvlpm\CommandBus\Application\HasScopeInterface;
use Dvlpm\CommandBus\Domain\Scope;

final class TestHasScopeCommand implements HasScopeInterface
{
    public function __construct(private Scope $scope)
    {
    }

    public function getScope(): Scope
    {
        return $this->scope;
    }
}
