<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Tests\Support\Builder;

use Dvlpm\CommandBus\Domain\Scope;

final class ScopeBuilder
{
    private \DateTimeImmutable $tryUntil;

    public function __construct()
    {
        $this->tryUntil = new \DateTimeImmutable(sprintf('+%d sec', 24 * 60 * 60));
    }

    public function withTryUntil(\DateTimeImmutable $tryUntil): self
    {
        $this->tryUntil = $tryUntil;

        return $this;
    }

    public static function create(): self
    {
        return new self();
    }

    public function build(): Scope
    {
        return new Scope($this->tryUntil);
    }
}
