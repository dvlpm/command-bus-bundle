<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Application\EventBus;

use Dvlpm\CommandBus\Domain\EventStoreInterface;

interface EventBusInterface
{
    public function dispatchFromStore(EventStoreInterface $eventStore): void;
}
