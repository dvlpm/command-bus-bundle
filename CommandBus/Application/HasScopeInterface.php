<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Application;

use Dvlpm\CommandBus\Domain\Scope;

interface HasScopeInterface
{
    public function getScope(): Scope;
}
