<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Application\CommandBus;

interface CommandBusInterface
{
    public function handle(object $command): void;
}
