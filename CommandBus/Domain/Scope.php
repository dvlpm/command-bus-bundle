<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Domain;

use DateTimeImmutable;

final class Scope
{
    private int $retryCount;
    private DateTimeImmutable $tryUntil;
    private DateTimeImmutable $requestedAt;

    public function __construct(DateTimeImmutable $tryUntil, ?DateTimeImmutable $requestedAt = null)
    {
        $this->retryCount = 0;
        $this->tryUntil = $tryUntil;
        $this->requestedAt = $requestedAt ?? new DateTimeImmutable();
    }

    public function incrementRetryCount(): void
    {
        ++$this->retryCount;
    }

    public function getRetryCount(): int
    {
        return $this->retryCount;
    }

    public function getTryUntil(): DateTimeImmutable
    {
        return $this->tryUntil;
    }

    public function getRequestedAt(): DateTimeImmutable
    {
        return $this->requestedAt;
    }
}
