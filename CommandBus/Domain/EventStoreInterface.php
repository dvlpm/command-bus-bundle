<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Domain;

interface EventStoreInterface
{
    /** @return EventInterface[] */
    public function flushEvents(): iterable;
}
