<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Adapter\Application\EventBus;

use Dvlpm\CommandBus\Application\EventBus\EventBusInterface;
use Dvlpm\CommandBus\Domain\EventStoreInterface;
use Dvlpm\CommandBus\Infrastructure\Messenger\Wrapper\MessengerDispatchWrapperTrait;
use Symfony\Component\Messenger\MessageBusInterface;

final class EventBus implements EventBusInterface
{
    use MessengerDispatchWrapperTrait;

    private MessageBusInterface $messageBus;
    private array $events = [];

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function dispatchFromStore(EventStoreInterface $eventStore): void
    {
        $this->events = [...$this->events, ...$eventStore->flushEvents()];
    }

    public function flush(): void
    {
        $eventsToDispatch = $this->events;
        $this->events = [];

        foreach ($eventsToDispatch as $event) {
            $this->dispatchWrapped($this->messageBus, $event);
        }
    }
}
