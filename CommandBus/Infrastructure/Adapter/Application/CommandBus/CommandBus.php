<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Adapter\Application\CommandBus;

use Dvlpm\CommandBus\Application\CommandBus\CommandBusInterface;
use Dvlpm\CommandBus\Infrastructure\Messenger\Wrapper\MessengerDispatchWrapperTrait;
use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBus implements CommandBusInterface
{
    use MessengerDispatchWrapperTrait;

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function handle(object $command): void
    {
        $this->dispatchWrapped($this->messageBus, $command);
    }
}
