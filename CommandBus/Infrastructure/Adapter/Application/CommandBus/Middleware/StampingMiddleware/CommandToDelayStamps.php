<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Adapter\Application\CommandBus\Middleware\StampingMiddleware;

use Dvlpm\CommandBus\Application\HasScopeInterface;
use Dvlpm\CommandBus\Infrastructure\Messenger\Middleware\StampingMiddleware\StampProducerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;

final class CommandToDelayStamps implements StampProducerInterface
{
    private array $commandsToDelay;

    public function __construct(array $commandsToDelay)
    {
        $this->commandsToDelay = $commandsToDelay;
    }

    public function produceStamps(Envelope $envelope): array
    {
        $delayMs = $this->resolveDelayForCommand($envelope->getMessage());
        if ($delayMs === null) {
            return [];
        }

        $delayMs = max(
            $delayMs,
            $this->resolveMaxDelayFromEnvelope($envelope),
        );

        return [
            new DelayStamp($delayMs),
        ];
    }

    private function resolveMaxDelayFromEnvelope(Envelope $envelope): int
    {
        return (int) max([
            0,
            ...array_map(
                fn (DelayStamp $stamp) => $stamp->getDelay(),
                $envelope->all(DelayStamp::class)
            ),
        ]);
    }

    private function resolveDelayForCommand(object $command): ?int
    {
        $commandClass = get_class($command);
        if (!isset($this->commandsToDelay[$commandClass])) {
            return null;
        }

        $delaySettings = $this->commandsToDelay[$commandClass];

        if ($command instanceof HasScopeInterface) {
            return $this->calculateDelayForScopedCommand($command, $delaySettings);
        }

        return (int) $delaySettings['delay'];
    }

    private function calculateDelayForScopedCommand(HasScopeInterface $command, array $delaySettings): int
    {
        $delayMs = (int) $delaySettings['delay'];
        $delayMultiplier = (int) $delaySettings['multiplier'];
        $maxDelayMs = (int) $delaySettings['max_delay'];

        $delayMs *= ($delayMultiplier ** $command->getScope()->getRetryCount());

        return $delayMs > $maxDelayMs
            ? $maxDelayMs
            : $delayMs;
    }
}
