<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Listener;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\RuntimeException;
use Symfony\Component\Messenger\Exception\UnrecoverableExceptionInterface;
use Symfony\Component\Messenger\Retry\RetryStrategyInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\RedeliveryStamp;
use Symfony\Component\Messenger\Transport\Sender\SenderInterface;

class SendFailedMessageForRetryListener implements EventSubscriberInterface
{
    private ContainerInterface $sendersLocator;
    private ContainerInterface $retryStrategyLocator;
    private LoggerInterface $logger;

    public function __construct(
        ContainerInterface $sendersLocator,
        ContainerInterface $retryStrategyLocator,
        LoggerInterface $logger = null
    ) {
        $this->sendersLocator = $sendersLocator;
        $this->retryStrategyLocator = $retryStrategyLocator;
        $this->logger = $logger ?? new NullLogger();
    }

    public function onMessageFailed(WorkerMessageFailedEvent $event): void
    {
        $retryStrategy = $this->getRetryStrategyForTransport($event->getReceiverName());
        $envelope = $event->getEnvelope();
        $throwable = $event->getThrowable();

        $message = $envelope->getMessage();
        $context = [
            'message' => $message,
            'class' => \get_class($message),
        ];

        $shouldRetry = $retryStrategy !== null && $this->shouldRetry($throwable, $envelope, $retryStrategy);

        $retryCount = RedeliveryStamp::getRetryCountFromEnvelope($envelope);
        if (!$shouldRetry) {
            $this->logger->critical(
                'Error thrown while handling message {class}. Removing from transport after {retryCount} retries. Error: "{error}"',
                $context + [
                    'retryCount' => $retryCount,
                    'error' => $throwable->getMessage(),
                    'exception' => $throwable,
                ]
            );

            return;
        }

        $event->setForRetry();

        ++$retryCount;

        $delay = $retryStrategy->getWaitingTime($envelope);
        $this->logger->error(
            'Error thrown while handling message {class}. Sending for retry #{retryCount} using {delay} ms delay. Error: "{error}"',
            $context + [
                'retryCount' => $retryCount,
                'delay' => $delay,
                'error' => $throwable->getMessage(),
                'exception' => $throwable,
            ]
        );

        if ($throwable instanceof HandlerFailedException) {
            $throwable = current($throwable->getNestedExceptions());
        }
        // add the delay and retry stamp info
        $retryEnvelope = $envelope->with(
            new DelayStamp($delay),
            new RedeliveryStamp(
                $retryCount,
                $throwable->getMessage(),
                FlattenException::createFromThrowable($throwable)
            )
        );

        // re-send the message for retry
        $this->getSenderForTransport($event->getReceiverName())->send($retryEnvelope);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // must have higher priority than SendFailedMessageToFailureTransportListener
            WorkerMessageFailedEvent::class => ['onMessageFailed', 100],
        ];
    }

    private function shouldRetry(\Throwable $e, Envelope $envelope, RetryStrategyInterface $retryStrategy): bool
    {
        // if ALL nested Exceptions are an instance of UnrecoverableExceptionInterface we should not retry
        if ($e instanceof HandlerFailedException) {
            $shouldNotRetry = true;
            foreach ($e->getNestedExceptions() as $nestedException) {
                if (!$nestedException instanceof UnrecoverableExceptionInterface) {
                    $shouldNotRetry = false;
                    break;
                }
            }
            if ($shouldNotRetry) {
                return false;
            }
        }

        if ($e instanceof UnrecoverableExceptionInterface) {
            return false;
        }

        return $retryStrategy->isRetryable($envelope);
    }

    private function getRetryStrategyForTransport(string $alias): ?RetryStrategyInterface
    {
        if ($this->retryStrategyLocator->has($alias)) {
            return $this->retryStrategyLocator->get($alias);
        }

        return null;
    }

    private function getSenderForTransport(string $alias): SenderInterface
    {
        if ($this->sendersLocator->has($alias)) {
            return $this->sendersLocator->get($alias);
        }

        throw new RuntimeException(sprintf('Could not find sender "%s" based on the same receiver to send the failed message to for retry.', $alias));
    }
}
