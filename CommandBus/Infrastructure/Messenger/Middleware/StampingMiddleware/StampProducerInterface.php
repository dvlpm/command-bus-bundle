<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Messenger\Middleware\StampingMiddleware;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\StampInterface;

interface StampProducerInterface
{
    /**
     * @param Envelope $envelope
     *
     * @return StampInterface[]
     */
    public function produceStamps(Envelope $envelope): array;
}
