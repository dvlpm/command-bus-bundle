<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Messenger\Middleware;

use Dvlpm\CommandBus\Infrastructure\Messenger\Middleware\StampingMiddleware\StampProducerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

final class StampingMiddleware implements MiddlewareInterface
{
    private StampProducerInterface $stampProducer;

    public function __construct(StampProducerInterface $stampProducer)
    {
        $this->stampProducer = $stampProducer;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $envelope = $this->maybeAddStamps($envelope);

        return $stack->next()->handle($envelope, $stack);
    }

    private function maybeAddStamps(Envelope $envelope): Envelope
    {
        $stamps = $this->stampProducer->produceStamps($envelope);
        $envelope = $envelope->with(...$stamps);

        return $envelope;
    }
}
