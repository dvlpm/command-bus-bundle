<?php

declare(strict_types=1);

namespace Dvlpm\CommandBus\Infrastructure\Messenger\Middleware;

use Dvlpm\CommandBus\Infrastructure\Adapter\Application\EventBus\EventBus;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

final class EventBusMiddleware implements MiddlewareInterface
{
    private EventBus $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $result = $stack->next()->handle($envelope, $stack);

        $this->eventBus->flush();

        return $result;
    }
}
