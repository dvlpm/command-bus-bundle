<?php

declare(strict_types=1);

namespace Dvlpm\CommandBusBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CommandBusBundle extends Bundle
{
}
